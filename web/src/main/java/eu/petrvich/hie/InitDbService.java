package eu.petrvich.hie;

import java.util.Calendar;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import eu.petrvich.hie.entity.Category;
import eu.petrvich.hie.entity.Item;
import eu.petrvich.hie.entity.ItemType;
import eu.petrvich.hie.entity.Person;
import eu.petrvich.hie.repository.CategoryRepository;
import eu.petrvich.hie.repository.ItemRepository;
import eu.petrvich.hie.repository.PersonRepository;

@Service
@Profile("dev")
public class InitDbService {

	@Autowired
	private ItemRepository itemRepostory;

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private PersonRepository personRepository;

	@PostConstruct
	public void init() {
		Category vajicka = addCategor(1, "Vajíčka");
		Category klobasy = addCategor(2, "Klobásy");
		Category brambory = addCategor(3, "Krůty");

		Person person = new Person();
		person.setId(1);
		person.setLogin("petr");
		person.setName("Petr");
		person.setSurname("Vích");
		person.setPassword("heslo");
		personRepository.save(person);

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 1);

		for (int i = 1; i < 9; i++) {
			cal.set(Calendar.MONTH, i % 12);
			if (i % 2 == 0) {
				expenditureItem(i, 3 * i, vajicka, person, cal.getTime());
				if (i % 3 == 0) {
					expenditureItem(i, 150 * i, klobasy, person, cal.getTime());
				}
				

			}

			if (i % 3 == 0) {
				expenditureItem(i, 5 * i, brambory, person, cal.getTime());
			}
		}

	}

	private Category addCategor(int id, String name) {
		Category cat = new Category();
		cat.setId(id);
		cat.setName(name);
		categoryRepository.save(cat);
		return cat;
	}

	private void expenditureItem(int count, int price, Category cat, Person person, Date date) {
		Item item = new Item();
		item.setCategory(cat);
		item.setCount(count);
		item.setPrice(price);
		item.setType(ItemType.INCOME);
		item.setDate(date);
		item.setDescription("toto je popis");
		item.setPerson(person);
		itemRepostory.save(item);
	}
}
