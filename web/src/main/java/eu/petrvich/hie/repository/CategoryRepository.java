package eu.petrvich.hie.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.petrvich.hie.entity.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {

}
