/**
 * 
 */
package eu.petrvich.hie.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.petrvich.hie.entity.Person;

/**
 * @author petr
 *
 */
public interface PersonRepository extends JpaRepository<Person, Long> {

}
