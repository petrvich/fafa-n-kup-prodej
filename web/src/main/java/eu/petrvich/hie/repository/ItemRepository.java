package eu.petrvich.hie.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import eu.petrvich.hie.entity.Item;
import eu.petrvich.hie.entity.ItemType;

public interface ItemRepository extends JpaRepository<Item, Long> {

	@Query("select i from Item i where i.date >= ?1 and i.date < ?2")
	List<Item> findAllBetweenDates(Date from, Date to);
	
	@Query("select i from Item i where i.date >= ?1 and i.date <= ?2 and i.type = ?3")
	List<Item> findAllBetweenDatesAndType(Date from, Date to, ItemType type);
}
