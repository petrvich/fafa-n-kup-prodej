package eu.petrvich.hie.dto.sumarry;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Total {

	YearSummary years;
	List<MonthSummary> months;

}
