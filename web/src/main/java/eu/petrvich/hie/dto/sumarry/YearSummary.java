package eu.petrvich.hie.dto.sumarry;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class YearSummary {

	private List<YearSummaryItem> years;
	private YearSummaryItem total;
}
