package eu.petrvich.hie.dto;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import eu.petrvich.hie.entity.Item;

@Getter
@Setter
@NoArgsConstructor
public class CategoryItemsDto {

	/**
	 * @param id
	 * @param name
	 * @param items
	 */
	public CategoryItemsDto(long id, String name, List<Item> items) {
		this.id = id;
		this.name = name;
		this.items = items;
	}
	
	/**
	 * @param name
	 * @param items
	 */
	public CategoryItemsDto(String name, List<Item> items) {
		this.name = name;
		this.items = items;
	}

	private long id;

	private String name;

	private List<Item> items;

}
