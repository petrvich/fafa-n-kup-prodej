package eu.petrvich.hie.dto.sumarry;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class MonthSummary {

	private List<CategorySummary> categories;
	private CategorySummary totalPerCategory;
	private String monthName;

}
