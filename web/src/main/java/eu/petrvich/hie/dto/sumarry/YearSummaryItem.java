package eu.petrvich.hie.dto.sumarry;

import lombok.Getter;
import lombok.AllArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class YearSummaryItem {
	
	private String year;
	private int income;
	private int outbound;
	private int total;

}
