package eu.petrvich.hie.dto.sumarry;

import eu.petrvich.hie.entity.Category;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CategorySummary {
	
	private Category category;
	private int income;
	private int outbound;
	private int price;

}
