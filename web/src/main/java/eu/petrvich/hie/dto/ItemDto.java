package eu.petrvich.hie.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import eu.petrvich.hie.entity.ItemType;

@Getter
@Setter
public class ItemDto {

	public ItemDto(ItemType type, Date date, String description, int price,
			int count) {
		this.type = type;
		this.date = date;
		this.description = description;
		this.price = price;
		this.count = count;
	}

	private long id;

	private ItemType type;

	private Date date;

	private String description;

	private int price;

	private int count;

}
