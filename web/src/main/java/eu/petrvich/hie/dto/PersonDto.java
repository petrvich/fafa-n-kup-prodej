package eu.petrvich.hie.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import eu.petrvich.hie.entity.Item;

/**
 * 
 * @author petr
 *
 */
@Getter
@Setter
public class PersonDto {

	private long id;

	private String name;

	private String surname;

	private String username;

	private List<Item> items;

	/**
	 * @param name
	 * @param surname
	 * @param username
	 * @param items
	 */
	public PersonDto(String name, String surname, String username,
			List<Item> items) {
		super();
		this.name = name;
		this.surname = surname;
		this.username = username;
		this.items = items;
	}

}
