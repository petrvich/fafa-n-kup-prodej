/**
 * 
 */
package eu.petrvich.hie.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author petr
 *
 */
@Getter
@Setter
public class CountedItem {
	
	private int incomeSum;
	
	private int outboundSum;
	
	private int totalCount;

}
