package eu.petrvich.hie.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eu.petrvich.hie.entity.Category;
import eu.petrvich.hie.repository.CategoryRepository;

@Service
public class CategoryService {

	@Autowired
	private CategoryRepository categpryRepository;
	
	public List<Category> getAll(){
		return this.categpryRepository.findAll();
	}
	
	public Category findById(long id){
		return this.findById(id);
	}
	
	public void save(Category category){
		this.categpryRepository.save(category);
	}
	
	public void delete(Category category){
		this.categpryRepository.delete(category);
	}
	
	public void delete(Long id){
		this.categpryRepository.delete(id);
	}
}
