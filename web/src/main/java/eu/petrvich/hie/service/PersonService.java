package eu.petrvich.hie.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eu.petrvich.hie.entity.Person;
import eu.petrvich.hie.repository.PersonRepository;

@Service
public class PersonService {
	
	@Autowired
	private PersonRepository personRepository;
	
	public void save(Person person){
		this.personRepository.save(person);
	}
	
	public void delete(Person person){
		this.personRepository.delete(person);
	}
	
	public void delete(Long id){
		this.personRepository.delete(id);
	}

}
