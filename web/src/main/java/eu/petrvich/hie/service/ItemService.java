package eu.petrvich.hie.service;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import eu.petrvich.hie.dto.CountedItem;
import eu.petrvich.hie.entity.Category;
import eu.petrvich.hie.entity.Item;
import eu.petrvich.hie.entity.ItemType;
import eu.petrvich.hie.repository.ItemRepository;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ItemService {

	@Autowired
	private ItemRepository itemRepository;

	public List<Item> getItemsPerLastTwoWeeks() {
		Calendar cal = Calendar.getInstance();
		Date to = cal.getTime();
		cal.add(Calendar.DAY_OF_MONTH, -14);
		Date from = cal.getTime();
		return itemRepository.findAllBetweenDates(from, to);
	}

	public List<Item> getAllBetweenDates(Date from, Date to) {
		return itemRepository.findAllBetweenDates(from, to);
	}

	public Map<Category, CountedItem> getAllCountedItemBetweenDates(int startMonth, int numberOfMonths, int year) {
		Calendar cal = Calendar.getInstance();
		cal.set(year, startMonth, 1);
		Date from = cal.getTime();
		cal.set(Calendar.MONTH, startMonth+numberOfMonths);
		Date to = cal.getTime();
		Map<Category, CountedItem> map = new HashMap<>();
		List<Item> readedList = itemRepository.findAllBetweenDates(from, to);
		log.info("počet záznamu z databáze = " + readedList.size());

		for (Item item : readedList) {
			int price = item.getPrice();

			CountedItem countedItem;
			if (map.containsKey(item.getCategory())) {
				countedItem = map.get(item.getCategory());
			} else {
				countedItem = new CountedItem();
			}

			if (item.getType().equals(ItemType.EXPENSE)) {
				countedItem.setOutboundSum(countedItem.getOutboundSum() + 1);
				countedItem.setTotalCount(countedItem.getTotalCount() - price);
			} else {
				countedItem.setIncomeSum(countedItem.getIncomeSum() + 1);
				countedItem.setTotalCount(countedItem.getTotalCount() + price);
			}

			map.put(item.getCategory(), countedItem);

		}

		log.info("počet záznamu v mapě = " + map.size());
		return map;
	}

	public Page<Item> readLastNRecords(int numberOfRecords) {
		return itemRepository.findAll(new PageRequest(0, numberOfRecords, new Sort("date")));
	}

	public List<Item> getAll() {
		return itemRepository.findAll();
	}

	public void save(Item item) {
		this.itemRepository.save(item);
	}

	public void delete(Item item) {
		this.itemRepository.delete(item);
	}

	public void delete(Long id) {
		this.itemRepository.delete(id);
	}
}
