package eu.petrvich.hie.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eu.petrvich.hie.dto.sumarry.YearSummary;
import eu.petrvich.hie.dto.sumarry.YearSummaryItem;
import eu.petrvich.hie.entity.Item;
import eu.petrvich.hie.entity.ItemType;
import eu.petrvich.hie.repository.ItemRepository;

@Service
public class ItemSummaryService {
	
	private static final int FIRST_DAY_IN_YEAR = 1;
	private static final int LAST_DAY_IN_YEAR = 31;

	@Autowired
	private ItemRepository itemRepository;

	public YearSummary getYearSummary(int yearFrom, int yearTo) {
		List<YearSummaryItem> items = this.getYearSummaryItemList(yearFrom, yearTo);
		int totalIncome = 0;
		int totalOutbound = 0;
		for (YearSummaryItem yearSummaryItem : items) {
			totalIncome += yearSummaryItem.getIncome();
			totalOutbound += yearSummaryItem.getOutbound();
		}

		int totalPrice = totalIncome - totalOutbound;
		YearSummaryItem total = new YearSummaryItem("Celkem", totalIncome, totalOutbound, totalPrice);
		return new YearSummary(items, total);
	}

	private List<YearSummaryItem> getYearSummaryItemList(int yearFrom, int yearTo) {
		List<YearSummaryItem> items = new ArrayList<>();
		for (int i = yearFrom; i <= yearTo; i++) {
			YearSummaryItem item = this.getItemSummaryItem(i);
			items.add(item);
		}
		return items;
	}

	private YearSummaryItem getItemSummaryItem(int year) {
		Calendar cal = Calendar.getInstance();
		cal.set(year, Calendar.JANUARY, FIRST_DAY_IN_YEAR);
		Date from = cal.getTime();
		cal.set(Calendar.MONTH, Calendar.DECEMBER);
		cal.set(Calendar.DAY_OF_MONTH, LAST_DAY_IN_YEAR);
		Date to = cal.getTime();

		List<Item> itemsIncome = itemRepository.findAllBetweenDatesAndType(from, to, ItemType.INCOME);
		List<Item> itemsOutbound = itemRepository.findAllBetweenDatesAndType(from, to, ItemType.EXPENSE);

		int sumIncome = this.calculatePriceFromItems(itemsIncome);
		int sumOutbound = this.calculatePriceFromItems(itemsOutbound);
		int sumTotal = sumIncome - sumOutbound;

		return new YearSummaryItem(Integer.toString(year), sumIncome, sumOutbound, sumTotal);
	}

	private int calculatePriceFromItems(List<Item> items) {
		int sum = 0;
		for (Item item : items) {
			sum += item.getPrice();
		}
		return sum;
	}
}
