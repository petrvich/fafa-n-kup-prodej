package eu.petrvich.hie;

import java.net.URI;
import java.net.URISyntaxException;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.zaxxer.hikari.HikariDataSource;

@Profile("prod")
@Configuration
public class ProdConfiguration {

	@Bean
	public DataSource dataSource() throws URISyntaxException{

		URI dbUri  = new URI("postgres://fwkuipcxeyzlds:UvO4Sv11Cnb2RvI444B8t8Imz_@ec2-54-235-145-226.compute-1.amazonaws.com:5432/d34tm05q36lg3a");
			String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + dbUri.getPath();

			HikariDataSource dataSource = new HikariDataSource();
			dataSource.setUsername(dbUri.getUserInfo().split(":")[0]);
			dataSource.setPassword(dbUri.getUserInfo().split(":")[1]);
			dataSource.setJdbcUrl(dbUrl);
//			dataSource.setDriverClassName("org.postgresql.ds.PGSimpleDataSource");
			return dataSource;
		

	}

}
