/**
 * 
 */
package eu.petrvich.hie.contoller.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import eu.petrvich.hie.entity.Category;
import eu.petrvich.hie.service.CategoryService;
import lombok.extern.slf4j.Slf4j;

/**
 * @author petr
 *
 */
@Slf4j
public class LongToCategoryConverter implements Converter<String, Category> {

	@Autowired
	private CategoryService categoryService;

	@Override
	public Category convert(String id) {
		log.info("parsing category id to category object for id = " + id);
		return categoryService.findById(Long.parseLong(id));
	}

}
