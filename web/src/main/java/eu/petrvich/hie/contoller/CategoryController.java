package eu.petrvich.hie.contoller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import eu.petrvich.hie.entity.Category;
import eu.petrvich.hie.service.CategoryService;

@Controller
@RequestMapping("/categories")
public class CategoryController {
	
	@Autowired
	private CategoryService categoryService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String show(Model model){
		Category category = new Category();
		List<Category> categories = categoryService.getAll();
		model.addAttribute("categories", categories);
		model.addAttribute("category", category);
		return "categories/categories-list";
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String add(@ModelAttribute @Valid Category category, BindingResult bindingResult,
			RedirectAttributes redirectAttributes) {
		if (bindingResult.hasErrors()) {
			return "categories/categories-list";
		}

		redirectAttributes.addFlashAttribute("success", true);
		categoryService.save(category);
		return "redirect:/categories";
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String delete(@RequestParam Long id, RedirectAttributes redirectAttributes) {
		redirectAttributes.addFlashAttribute("success", true);
		categoryService.delete(id);
		return "redirect:/items/add";
	}

}
