package eu.petrvich.hie.contoller;

import java.util.Calendar;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import eu.petrvich.hie.entity.Item;
import eu.petrvich.hie.service.CategoryService;
import eu.petrvich.hie.service.ItemService;
import eu.petrvich.hie.service.ItemSummaryService;

@Controller
@RequestMapping(value = { "/", "/items" })
public class ItemController {

	private static final int NUMBER_OF_LAST_RECORS = 5000;
	private static final int START_YEAR = 2014;
	private static final int END_YEAR = 2020;

	@Autowired
	private ItemService itemService;
	
	@Autowired
	private ItemSummaryService itemSummaryService;

	@Autowired
	private CategoryService categoryService;

	@RequestMapping(method = RequestMethod.GET)
	public String getAll(Model model) {
		
		model.addAttribute("yearSummary", itemSummaryService.getYearSummary(START_YEAR, END_YEAR));

		return "items/items-list";
	}

	@RequestMapping("/add")
	public String showAdd(Model model) {
		Calendar cal = Calendar.getInstance();
		Item item = new Item();
		item.setDate(cal.getTime());
		model.addAttribute(item);
		model.addAttribute("categories", categoryService.getAll());

		Page<Item> lastItems = this.readLastItems(NUMBER_OF_LAST_RECORS);
		model.addAttribute("lastItems", lastItems);
		return "items/item-form";
	}

	private Page<Item> readLastItems(int numberOfRecords) {
		return itemService.readLastNRecords(numberOfRecords);
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String add(@ModelAttribute @Valid Item item, BindingResult bindingResult,
			RedirectAttributes redirectAttributes) {
		if (bindingResult.hasErrors()) {
			return "items/item-form";
		}

		redirectAttributes.addFlashAttribute("success", true);
		itemService.save(item);
		return "redirect:/items/add";
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String delete(@RequestParam Long id, RedirectAttributes redirectAttributes) {
		redirectAttributes.addFlashAttribute("success", true);
		itemService.delete(id);
		return "redirect:/items/add";
	}
}
