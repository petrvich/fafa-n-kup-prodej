/**
 * 
 */
package eu.petrvich.hie.contoller.converter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.core.convert.converter.Converter;

/**
 * @author petr
 *
 */
public class StringToDateConverter implements Converter<String, Date> {

	@Override
	public Date convert(String arg0) {
		DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
		
		try {
			return formatter.parse(arg0);
		} catch (ParseException e) {
			return Calendar.getInstance().getTime();
		}
	}

}
