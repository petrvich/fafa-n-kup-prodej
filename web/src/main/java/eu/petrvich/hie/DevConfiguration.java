package eu.petrvich.hie;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.zaxxer.hikari.HikariDataSource;

@Profile("dev")
@Configuration
public class DevConfiguration {
	
	@Bean
	public DataSource dataSource() {
		HikariDataSource dataSource = new HikariDataSource();
		dataSource.setUsername("sa");
		dataSource.setPassword("");
		dataSource.setJdbcUrl("jdbc:hsqldb:mem:test");
		return dataSource;
	}
	

}
