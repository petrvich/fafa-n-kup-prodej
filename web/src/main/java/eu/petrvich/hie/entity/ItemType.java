package eu.petrvich.hie.entity;

import lombok.Getter;

@Getter
public enum ItemType {

	INCOME("Příjem"), EXPENSE("Výdaj");

	private String value;

	private ItemType(String value) {
		this.value = value;
	}

}
